/**
 * Class represents a single currency bid.
 */
public class CurrencyBid {

    private Currency currency;
    private double bid;

    /**
     * Get currency.
     * @return currency
     */
    public Currency getCurrency() {
        return currency;
    }

    /**
     * Set currency.
     * @param currency
     */
    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    /**
     * Get currency bid.
     * @return bid A single bid of the currency.
     */
    public double getBid() {
        return bid;
    }

    /**
     * Set currency bid.
     * @param bid A single bid of the currency.
     */
    public void setBid(double bid) {
        this.bid = bid;
    }

    /**
     * Prints information about the object.
     */
    @Override
    public String toString() {
        return this.getClass().getName() + "[currency = " + currency + ", bid = " + bid + "]";
    }

}
