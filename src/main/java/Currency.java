/**
 * Class represents a single currency object.
 */
public class Currency {

    private String code;
    private String name;

    /**
     * An empty constructor.
     */
    public Currency() {

    }

    /**
     * Constructs a currency object.
     *
     * @param code A currency code.
     * @param name A currency name.
     */
    public Currency(String code, String name) {
        this.code = code;
        this.name = name;
    }

    /**
     * Get currency code.
     * @return code A code of the currency.
     */
    public String getCode() {
        return code;
    }

    /**
     * Set currency code.
     * @param code A code of the currency.
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Get currency name.
     * @return name A name of the currency.
     */
    public String getName() {
        return name;
    }

    /**
     * Set currency code.
     * @param name A name of the currency.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Prints information about the object.
     */
    @Override
    public String toString() {
        return this.getClass().getName() + "[code = " + code + ", name = " + name + "]";
    }

}