
import java.io.IOException;

import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;


import com.fasterxml.jackson.core.*;


import static java.sql.Date.valueOf;

/**
 * A class retrieves and processes a gold data from NBP API.
 */
public class GoldPriceRates {


    /**
     * Return gold prices from between given dates
     * @param date date (or days) from which the prices are to be returned
     * @return ArrayList<GoldPrice> a list of gold prices.
     * @throws CurrencyReadingException
     */


    public static ArrayList<GoldPrice> getGoldPriceDate(LocalDate... date) throws CurrencyReadingException{
        if (date.length >= 3) {
            throw new CurrencyReadingException("Too many dates were given");
        }
        if (date.length == 2) {
            if (date[0].compareTo(date[1]) > 0) {
                throw new CurrencyReadingException("The 'From' date must be before the 'to' date.");
            }

            if(date[1].isAfter(LocalDate.now())){
                throw new CurrencyReadingException("You can't get data from the future");
            }

        }
        ArrayList<GoldPrice> goldPrices = new ArrayList<>();
        while(ChronoUnit.DAYS.between(date[0],date[1]) > 91){
            LocalDate date1 = date[0].plusDays(91);
            String call = "http://api.nbp.pl/api/cenyzlota/";
            call = call + date[0].toString() + "/";
            call = call + date1.toString() + "/";
            call = call + "?format=json";
            goldPrices.addAll(getGoldPriceFromCall(call));
            date[0] = date1.plusDays(1);
        }
        String call = "http://api.nbp.pl/api/cenyzlota/";
        call = call + date[0].toString() + "/";
        call = call + date[1].toString() + "/";
        call = call + "?format=json";
        goldPrices.addAll(getGoldPriceFromCall(call));

        return goldPrices;
    }
    /**
     * Get list of gold prices form NBP API.
     * @param call a specific call for API
     * @return ArrayList<GoldPrice> a list of gold prices.
     * @throws CurrencyReadingException
     */

    private static ArrayList<GoldPrice> getGoldPriceFromCall(String call) throws CurrencyReadingException {
        JsonFactory jf = new JsonFactory();
        ArrayList<GoldPrice> goldPrices = new ArrayList<GoldPrice>();
        GoldPrice goldPrice = new GoldPrice();
        try {
            URL url = new URL(call);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod("GET");
            String fn;
            JsonParser jsonParser = jf.createParser(url);

            while (jsonParser.nextToken() != JsonToken.END_ARRAY) {
                fn = jsonParser.getCurrentName();

                if (jsonParser.getCurrentToken() == JsonToken.START_OBJECT) {
                    goldPrice = new GoldPrice();
                } else if (jsonParser.getCurrentToken() == JsonToken.END_OBJECT) {
                    goldPrices.add(goldPrice);
                } else {
                    if ("data".equals(fn)) {
                        jsonParser.nextToken();
                        goldPrice.setDate(toLocalDate(jsonParser.getText()));
                    } else if ("cena".equals(fn)) {
                        jsonParser.nextToken();
                        goldPrice.setPrice(Double.parseDouble(jsonParser.getValueAsString()));
                    }
                }
            }
            return goldPrices;


        } catch (Exception e) {
            throw new CurrencyReadingException("Error while parsing data: " + e.getMessage());
        }
    }
    private static LocalDate toLocalDate(String date) {

        String[] d = date.split("-");
        return LocalDate.of(
                Integer.parseInt(d[0]), Integer.parseInt(d[1]), Integer.parseInt(d[2])
        );

    }
}