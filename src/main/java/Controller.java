


import javafx.fxml.FXML;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;

import java.time.LocalDate;
import java.util.*;

public class Controller {

    @FXML
    LineChart<String,Double> linechart1;
    @FXML
    LineChart<String,Double> linechart3;
    @FXML
    DatePicker date_from;
    @FXML
    private CategoryAxis xAxis1;
    @FXML
    private CategoryAxis xAxis2;
    @FXML
    private TextArea error_text;
    @FXML
    DatePicker date_to;
    Map<String, XYChart.Series<String, Double>> CHARTS = new HashMap<>();
    @FXML
    CheckBox Gold,THB,USD,AUD,HKD,CAD,NZD,SGD,EUR,HUF,CHF,GBP,UAH,JPY,CZK,DKK,ISK,NOK,SEK,HRK,RON,BGN,TRY,ILS,CLP,PHP,MXN,ZAR,BRL,MYR,RUB,IDR,INR,KRW,CNY,XDR;
    List<CheckBox> Currencies = new ArrayList<>();


    /**
     * This function draws given currencies/ gold prices on the chart.
     */
    public void btn(javafx.event.ActionEvent actionEvent) throws CurrencyReadingException {
        error_text.setText("");
        error_text.setOpacity(0);
        try {

            List<CheckBox> Currencies = Arrays.asList(THB, USD, AUD, HKD, CAD, NZD, SGD, EUR, HUF, CHF, GBP, UAH, JPY, CZK, DKK, ISK, NOK, SEK, HRK, RON, BGN, TRY, ILS, CLP, PHP, MXN, ZAR, BRL, MYR, RUB, IDR, INR, KRW, CNY, XDR);
            linechart1.getData().clear();
            linechart3.getData().clear();
            if (Gold.isSelected()) {
                if(linechart3.getData().isEmpty()) {
                    XYChart.Series<String, Double> data = new XYChart.Series<String, Double>();
                    ArrayList<GoldPrice> gold = GoldPriceRates.getGoldPriceDate(date_from.getValue(), date_to.getValue());
                    for (GoldPrice GoldPrice : gold) {
                        data.getData().add(new XYChart.Data<String, Double>(GoldPrice.getDate().toString(), GoldPrice.getPrice()));

                    }
                    data.setName("Gold");
                    CHARTS.put("Gold", data);
                    linechart3.getData().add(data);
                    linechart3.setCreateSymbols(false);
                    xAxis2.setAnimated(false);
                }
                else{

                }
            }
            if(!Gold.isSelected()){
                linechart3.getData().clear();
            }
            for (CheckBox currency : Currencies) {
                if (currency.isSelected()) {
                    XYChart.Series<String, Double> data = new XYChart.Series<String, Double>();
                    CurrencyReading Currency = new CurrencyReading(currency.getId().toLowerCase(), date_from.getValue(), date_to.getValue());
                    Currency.parse();
                    List<Rate> Values = Currency.parse().getRates();
                    for (Rate value : Values) {
                        data.getData().add(new XYChart.Data<String, Double>(value.getDate().toString(), value.getBid()));
                    }
                    data.setName(Currency.getCurrencyTrading().getCurrency().getName());
                    CHARTS.put(Currency.getCurrencyTrading().getCurrency().getName(), data);
                    linechart1.getData().add(data);
                    linechart1.setCreateSymbols(false);
                    xAxis1.setAnimated(false);
                }

            }
        }catch (CurrencyReadingException  e){
            error_text.setText( e.getMessage());
            error_text.setOpacity(1);
        }
    }
    /**
     * This function clears the chart.
     */
    public void btn2(javafx.event.ActionEvent actionEvent) {
        for(CheckBox CheckBox:Currencies){
            CheckBox.setSelected(false);
        }
        Gold.setSelected(false);
        linechart1.getData().clear();
        linechart3.getData().clear();
    }


}