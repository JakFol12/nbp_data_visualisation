import java.time.LocalDate;
import java.util.Date;

/**
 * Class represents a gold price on a given day.
 */
public class GoldPrice {
    private LocalDate date;
    private Double price;

    /**
     * A constructor creating an object.
     * @param date A specified date of a gold price.
     * @param price a price of gold on a given day.
     */
    public GoldPrice(LocalDate date, Double price) {
        this.date = date;
        this.price = price;
    }

    /**
     * An empty constructor.
     */
    public GoldPrice(){

    }

    /**
     * Get date.
     * @return date A date of a specified gold prize.
     */
    public LocalDate getDate() {
        return date;
    }

    /**
     * Set date.
     * @param date A date of a specified gold prize.
     */
    public void setDate(LocalDate date) {
        this.date = date;
    }

    /**
     * Get price.
     * @return price A price of gold on a specified date.
     */
    public Double getPrice() {
        return price;
    }

    /**
     * Set price.
     * @param price A price of gold on a specified date.
     */
    public void setPrice(Double price) {
        this.price= price;
    }

    /**
     * Prints information about the object.
     */
    @Override
    public String toString() {
        return "GoldPrice{" +
                "date=" + date +
                ", price=" + price +
                '}';
    }
}