/**
 * An exception class for invalid currency data objects.
 */
public class CurrencyReadingException extends Exception{
    public CurrencyReadingException(String message) {
        super(message);
    }

}
