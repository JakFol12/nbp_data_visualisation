
import java.math.BigDecimal;
import java.util.Date;

/**
 * Class represents a currency price on a given day.
 */
public class CurrencyPrice {
    private String CURRENCY_NAME = null;
    private Date date;
    private BigDecimal price;

    /**
     * Constructs an object.
     *
     * @param date A specified date.
     * @param price A price of currency on specified date.
     */
    public CurrencyPrice(Date date, BigDecimal price) {
        this.date = date;
        this.price = price;
    }


    /**
     * Get currency name.
     * @return CURRENCY_NAME A name of the currency.
     */
    public String getCURRENCY_NAME() {
        return CURRENCY_NAME;
    }

    /**
     * Set currency name.
     * @param CURRENCY_NAME A name of the currency.
     */
    public void setCURRENCY_NAME(String CURRENCY_NAME) {
        this.CURRENCY_NAME = CURRENCY_NAME;
    }

    /**
     * Get date.
     * @return date A specified date.
     */
    public Date getDate() {
        return date;
    }

    /**
     * Set date.
     * @param date A specified date.
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Get price.
     * @return price A price of the currency on a specified date.
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * Set price.
     * @param price A price of the currency on a specified date.
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
