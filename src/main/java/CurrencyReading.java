import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import static java.time.temporal.ChronoUnit.DAYS;

/**
 * Class retrieves and processes a currency data from NBP API.
 */
public class CurrencyReading {

    private final String currency;
    private final LocalDate from;
    private final LocalDate to;
    private final String url = "http://api.nbp.pl/api/exchangerates/rates/a/_CURRENCY_/_FROM_/_TO_/?format=json";
    private CurrencyTrading trading = new CurrencyTrading();

    /**
     * Constructs an object that parse and processes data.
     *
     * @param currency a currency code.
     * @param from a start date
     * @param to an end date
     * @throws CurrencyReadingException
     */
    public CurrencyReading(String currency, LocalDate from, LocalDate to) throws CurrencyReadingException {

        if (currency == null || currency.isEmpty()) {
            throw new CurrencyReadingException("Currency code can not be empty.");
        }

        if (from == null || to == null) {
            throw new CurrencyReadingException("Dates can not be empty.");
        }

        if (!from.isBefore(to)) {
            throw new CurrencyReadingException("The 'From' date must be before the 'to' date.");
        }

        if(DAYS.between(from, to) >= 91){
            throw new CurrencyReadingException("You can't get data from more than 91 days");
        }

        if (from.isAfter(LocalDate.now()) || to.isAfter(LocalDate.now())) {
            throw new CurrencyReadingException("You can't get a data from future dates.");
        }

        this.currency = currency;
        this.from = from;
        this.to= to;
    }

    /**
     * Getting data parsed from NBP API by parse() method.
     * @return trading A result of parsing data.
     */
    public CurrencyTrading getCurrencyTrading() {
        return trading;
    }

    /**
     * Parsing date to LocalDate object.
     * @param date A date to be parsed.
     * @return date parsed.
     */
    private LocalDate toLocalDate(String date) {

        String[] d = date.split("-");
        return LocalDate.of(
                Integer.parseInt(d[0]), Integer.parseInt(d[1]), Integer.parseInt(d[2])
        );

    }

    /**
     * Parsing a json from NBP API.
     * @return trading A parsed data from NBP API.
     * @throws CurrencyReadingException
     */
    public CurrencyTrading parse() throws CurrencyReadingException {

        String _url = url.replaceAll("_CURRENCY_", currency)
                .replaceAll("_FROM_", from.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")))
                .replaceAll("_TO_", to.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));

        JsonFactory jf = new JsonFactory();

        try {
            JsonParser jp = jf.createJsonParser(new URL(_url));

            Currency curr = new Currency();

            List<Rate> rl = new ArrayList<>();

            while (jp.nextToken() != JsonToken.END_OBJECT) {

                String fn = jp.getCurrentName();
                if ("currency".equals(fn)) {
                    jp.nextToken();
                    curr.setName(jp.getText());
                } else if ("code".equals(fn)) {
                    jp.nextToken();
                    curr.setCode(jp.getText());
                } if ("rates".equals(fn)) {
                    jp.nextToken();
                    Rate rate = new Rate();
                    while (jp.nextToken() != JsonToken.END_ARRAY) {
                        fn = jp.getCurrentName();

                        if (jp.getCurrentToken() == JsonToken.START_OBJECT) {
                            rate = new Rate();
                        } else if (jp.getCurrentToken() == JsonToken.END_OBJECT) {
                            rl.add(rate);
                        } else {
                            if ("effectiveDate".equals(fn)) {
                                jp.nextToken();
                                rate.setDate(toLocalDate(jp.getText()));
                            } else if ("mid".equals(fn)) {
                                jp.nextToken();
                                rate.setBid(Double.parseDouble(jp.getText()));
                            }
                        }
                    }
                }
            }
            trading.setCurrency(curr);
            trading.setRates(rl);
            return trading;
        } catch (IOException e) {
            throw new CurrencyReadingException("Error while parsing data: " + e.getMessage());
        }
    }

}