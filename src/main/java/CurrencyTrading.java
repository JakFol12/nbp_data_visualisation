import java.util.List;

/**
 * Class for trading currency rates. It stores currency object and rates
 * from specified data time.
 */
public class CurrencyTrading {


    private Currency currency;
    private List<Rate> rates;


    /**
     * An empty constructor.
     */
    public CurrencyTrading() {
    }

    /**
     * A constructor creating object
     * @param currency A currency object.
     * @param rates a list of rates.
     */
    public CurrencyTrading(Currency currency,List<Rate> rates) {
        this.currency = currency;
        this.rates = rates;
    }

    /**
     * Get currency.
     * @return currency A currency object.
     */
    public Currency getCurrency() {
        return currency;
    }

    /**
     * Set currency.
     * @param currency A currency object.
     */
    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    /**
     * Get rates.
     * @return A list of rates.
     */
    public List<Rate> getRates() {
        return rates;
    }

    /**
     * Set rates.
     * @param rates A. list of rates
     */
    public void setRates(List<Rate> rates) {
        this.rates = rates;
    }

    /**
     * Prints information about the object.
     */
    @Override
    public String toString() {
        return this.getClass().getName() + "[currency = " + currency + ", rates = " + rates + "]";
    }

}