import java.time.LocalDate;

/**
 * A class stores info about single currency rate - a date and a bid.
 */
public class Rate {

    private LocalDate date;
    private double bid;

    /**
     * An empty constructor.
     */
    public Rate() {

    }

    /**
     * Constructs a rete object.
     *
     * @param date a rate date
     * @param bid
     */
    public Rate(LocalDate date, double bid) {
        this.date = date;
        this.bid = bid;
    }

    /**
     * Get date.
     * @return date A date of a rate.
     */
    public LocalDate getDate() {
        return date;
    }

    /**
     * Set date.
     * @param date A date of a rate.
     */
    public void setDate(LocalDate date) {
        this.date = date;
    }

    /**
     * Get bid.
     * @return bid A bid of a currency on specified date.
     */
    public double getBid() {
        return bid;
    }

    /**
     * Set bid.
     * @param  bid A bid of a currency on specified date.
     */
    public void setBid(double bid) {
        this.bid = bid;
    }

    /**
     * Prints information about the object.
     */
    @Override
    public String toString() {
        return this.getClass().getName() + "[date = " + date + ", bid = " + bid + "]";
    }

}