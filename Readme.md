# NBP Currency Ratings Visualisation
*Authors: Paulina Jaszczuk, Jakub Fołtyn, Jan Gąska*
## Folder structure:
* `/main/resources/`
sample.fxml file defining GUI layout
* `/main/java/`
Application structure


![currency plot](screenshots/1.png)
![gold plot](screenshots/2.png)
![both plots](screenshots/3.png)
![error message](screenshots/4.png)



